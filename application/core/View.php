<?
class View
{
    /**
     * Создаёт контент для вывода
     *
     * @param $contentView
     * @param $templateView
     * @param null $data
     */
    function generate($contentView, $templateView, $data = null)
    {
        include 'application/views/'.$templateView;
    }
}