<?
require_once 'dbConfig.php';

class Connector
{
    public static function connect()
    {
        $dbData = $GLOBALS['db'];
        $dbObject = new mysqli('localhost', $dbData['userName'], $dbData['dbPass'], $dbData['dbName']);
        if ($dbObject->connect_errno) {
            echo "Не удалось подключиться к MySQL: (" . $dbObject->connect_errno . ") " . $dbObject->connect_error;
            die();
        }

        unset($GLOBALS['db']);

        return $dbObject;
    }
}