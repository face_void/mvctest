<?
class Route
{
    /**
     * Базовая функция маршрутизации URL
     * Подключает нужный шаблон в соответствии с адресом обращения
     */
    public static function start()
    {
        $controllerName = 'Main';
        $actionName = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // получаем имя контроллера
        if ( !empty($routes[1]) ) {
            $controllerName = self::getValidName($routes[1]);
        }

        // получаем имя действия
        if ( !empty($routes[2]) ) {
            // убираем гет параметры из запроса, чтобы получить название действия
            $arAction = explode('?', $routes[2]);
            $actionName = self::getValidName($arAction[0]);
        }

        // добавляем префиксы
        $modelName = 'Model'.$controllerName;
        $controllerName = 'Controller'.$controllerName;
        $actionName = 'action'.$actionName;

        // подцепляем файл с классом модели (файла модели может и не быть)
        $modelFile = $modelName.'.php';
        $modelPath = "application/models/".$modelFile;
        if (file_exists($modelPath)) {
            include "application/models/".$modelFile;
        }

        // подцепляем файл с классом контроллера
        $controllerFile = $controllerName.'.php';
        $controllerPath = "application/controllers/".$controllerFile;
        if (file_exists($controllerPath)) {
            include "application/controllers/".$controllerFile;
        } else {
            Route::errorPage404();
        }

        // создаем контроллер
        $controller = new $controllerName;
        $action = $actionName;

        if(method_exists($controller, $action)) {
            $controller->$action();
        } else {
            Route::errorPage404();
        }

    }

    private static function errorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }


    /**
     * Возвращает постфикс класса для подключения в валидном виде
     *
     * @param $name
     * @return string
     */
    private static function getValidName($name)
    {
        return ucfirst($name);
    }
}