<?
class Validator
{
    /**
     * Проводит валидацию email
     *
     * @param $email
     * @return mixed
     */
    public static function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}