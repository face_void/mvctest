<?
class ControllerMain extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->model = new ModelMain();
    }

    /**
     * Получает данные для работы страницы и выводит шаблон
     */
    function actionIndex()
    {
        global $APP;
        $APP = $this;

        $this->view->generate('viewMain.php', 'template/template.php');
    }

    /**
     * Создаёт новую задачу
     */
    public function actionCreateTask()
    {
        try {
            $result = $this->model->createTask();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        echo json_encode($result);
    }

    /**
     * Получает список задач в html виде
     */
    public function getTasks()
    {
        try {
            $data = $this->model->getTasks();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        $this->view->generate('', $this->getTaskListTemplate(), $data);
    }

    /**
     * Редактирование заявки
     */
    public function actionEditTask()
    {
        try {
            $result = $this->model->editTask();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        echo json_encode($result);

    }

    /**
     * Устанавливает порядок и поле для сортировки
     */
    public function actionTaskSort()
    {
        $request = $this->prepareRequest($_REQUEST['data']);
        if (!empty($request['sortField'])) {
            $arSort = explode('|', $request['sortField']);
            $_SESSION['taskSort'] = $arSort[0];
            $_SESSION['taskSortDirection'] = $arSort[1];
        }

        $this->getTasks();
    }

    /**
     * Возвращает название шаблона для списка задач
     *
     * @return string
     */
    protected function getTaskListTemplate()
    {
        return ($_SESSION['user']['admin'] == 'Y') ? 'template/pageBlocks/taskListAdmin.php' : 'template/pageBlocks/taskList.php';
    }

    /**
     * Возвращает следующую страницу списка задач
     */
    public function actionNextPage() {
        $_SESSION['taskCurPage']++;

        $this->getTasks();
    }

    /**
     * Возвращает предыдущую страницу списка задач
     */
    public function actionPrevPage() {
        $_SESSION['taskCurPage']--;

        $this->getTasks();
    }

    /**
     * Возвращает конкретную страницу списка задач
     */
    public function actionSetPage() {
        $request =  json_decode($_REQUEST['data'], 1);
        $_SESSION['taskCurPage'] = htmlspecialchars($request['page']);

        $this->getTasks();
    }

    /**
     * Авторизация
     */
    public function actionAuthorize()
    {
        try {
            $result = $this->model->authorize();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        echo json_encode($result);
    }

    /**
     * Отмена авторизации
     */
    public function actionLogout()
    {
        try {
            $result = $this->model->logout();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        echo json_encode($result);
    }

    protected function prepareRequest($data)
    {
        return $this->model->prepareRequest($data);
    }
}