<div class="container">
    <div class="row">
        <h1 class="h1">Список задач</h1>
    </div>
    <div class="row justify-content-end buttons">
        <button id="createTask" class="button">Создать задачу</button>
        <?if ($_SESSION['user']['admin'] == 'Y'):?>
            <button id="logout" class="button admin">Выйти</button>
        <?else:?>
            <button id="admin" class="button admin">Авторизоваться</button>
        <?endif?>
    </div>
    <div class="container taskList">
        <div class="errorMessageMain"></div>
        <?
        global $APP;
        $APP->getTasks();
        ?>
    </div>
</div>