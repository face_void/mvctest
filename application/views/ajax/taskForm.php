<div class="container taskForm__wrap appended">
    <form action="/main/createTask" name="createTask" class="taskForm row">
        <div class="col-sm-12 errorMessage"></div>
        <div class="col-sm-4">
            <input type="text" name="userName" placeholder="Имя пользователя">
            <input type="text" name="userEmail" placeholder="Email">
            <button type="submit" class="button">Создать</button>
        </div>
        <div class="col-sm-8">
            <textarea name="taskText" placeholder="Описание задачи"></textarea>
        </div>
    </form>
    <div class="close">X</div>
</div>