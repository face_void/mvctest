$(document).ready(function () {
    // хостинг постоянно напоминает о том, что надо заплатить. скрыл
    $('div').first().css('display', 'none');

    // вызов формы "создать заявку"
    $(document).on('click', '#createTask', function () {
        var parent = $(this).parent();

        if (parent.find('[name=createTask]').length > 0)
            return;

        getTemplate('taskForm').then(
            function (result) {
                parent.append(result);
            }
        );
    });

    // создание заявки
    $(document).on('submit', '[name=createTask]', function (e) {
        e.preventDefault();

        var form = $(this),
            container = form.closest('.appended'),
            htmlForm = form[0],
            data = new FormData(htmlForm),
            json = formDataToJson(data),
            errorField = $(form).find('.errorMessage');

        form.addClass('processing');
        ajaxCommand(form.attr('action'), json, 'json').then(
            function(result) {
                form.removeClass('processing');

                // убираем сообщения об ошибках
                $(form).find('.error').removeClass('error');
                errorField.text('');

                // обработка ошибок
                if (result['error'] == true) {

                    // отсутсвуют необходимые поля
                    if (result['missedFields'].length > 0) {
                        errorField.append('<p>Необходимо заполнить все поля</p>');

                        result['missedFields'].forEach(function (field) {
                            $(form).find('[name=' + field + ']').addClass('error');
                        });
                    }

                    // поля не прошли валидацию
                    if (result['notValidFields'].length > 0) {
                        errorField.append('<p>Не верный формат email</p>');

                        result['missedFields'].forEach(function (field) {
                            $(form).find('[name=' + field + ']').addClass('error');
                        });
                    }
                } else {
                    // если всё в порядке
                    form.replaceWith('<div class="row justify-content-sm-center success-send">Задача ID ' + result['taskId'] + ' успешно создана</div>');
                    setTimeout(function () {
                        container.fadeOut(400);
                        setTimeout(function () {
                            location.reload();
                        }, 450);
                    }, 1000);
                }
            }
        );
    });

    // вызов формы "авторизация"
    $(document).on('click', '#admin', function () {
        var parent = $(this).parent();

        if (parent.find('[name=admin]').length > 0)
            return;

        getTemplate('adminForm').then(
            function (result) {
                parent.append(result);
            }
        );
    });

    // авторизация
    $(document).on('submit', '[name=admin]', function (e) {
        e.preventDefault();

        var form = $(this),
            container = form.closest('.appended'),
            htmlForm = form[0],
            data = new FormData(htmlForm),
            json = formDataToJson(data),
            errorField = $(form).find('.errorMessage');

        form.addClass('processing');
        ajaxCommand(form.attr('action'), json, 'json').then(
            function(result) {
                form.removeClass('processing');

                // убираем сообщения об ошибках
                $(form).find('.error').removeClass('error');
                errorField.text('');

                // обработка ошибок
                if (result['error'] == true) {

                    // отсутсвуют необходимые поля
                    if (result['missedFields'].length > 0) {
                        errorField.append('<p>Необходимо заполнить все поля</p>');

                        result['missedFields'].forEach(function (field) {
                            $(form).find('[name=' + field + ']').addClass('error');
                        });
                    }

                    // другие ошибки
                    if (result['message'].length > 0) {
                        result['message'].forEach(function (field) {
                            errorField.append('<p>' + field + '</p>');
                        });
                    }
                } else {
                    // если всё в порядке
                    form.replaceWith('<div class="row justify-content-sm-center success-send">Вы были успешно авторизованы</div>');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            }
        );
    });

    // отмена авторизации
    $(document).on('click', '#logout', function (e) {
        e.preventDefault();

        ajaxCommand('/main/logout', '', 'json').then(
            function (result) {
                if (result['error'] == false) {
                    location.reload();
                }
            }
        );
    });

    // редактирование заявки
    $(document).on('submit', '[name=editTask]', function (e) {
        e.preventDefault();

        var form = $(this),
            htmlForm = form[0],
            data = new FormData(htmlForm),
            json = formDataToJson(data),
            errorField = $('.errorMessageMain');

        ajaxCommand(form.attr('action'), json, 'json').then(
            function(result) {

                // убираем сообщения об ошибках
                errorField.html('');

                // обработка ошибок
                if (result['error'] == true) {

                    // другие ошибки
                    if (result['message'].length > 0) {
                        result['message'].forEach(function (field) {
                            errorField.append('<p>' + field + '</p>');
                        });
                    }
                } else {
                    // если всё в порядке
                    location.reload();
                }
            }
        );
    });

    // сортировка
    $(document).on('change', '.js-sort', function (e) {
        var form = $(this).closest('form'),
            htmlForm = form[0],
            data = new FormData(htmlForm),
            json = formDataToJson(data);

        ajaxCommand(form.attr('action'), json, 'html').then(
            function(result) {
                $('.taskList').html(result);
            }
        );
    });

    // пагинация
    $(document).on('click', '.pagination__item', function (e) {
        e.preventDefault();

        var taskList = $('.taskList'),
            page = $(this).text();

        ajaxCommand($(this).attr('href'), JSON.stringify({page: page}), 'html').then(
            function (result) {
                taskList.html(result);
            }
        );
    });

    // все кнопки закрытия окон
    $(document).on('click', '.close', function () {
        $(this).closest('.appended').remove();
    });
});

function getTemplate(template) {
    return $.ajax({
        url: '/application/views/ajax/' + template + '.php',
        dataType: 'html'
    });
}

function ajaxCommand(method, data, dataType) {
    return $.ajax({
        url: method,
        data: 'data=' + data,
        contentType: "application/json; charset=utf-8",
        dataType: dataType
    });
}

function formDataToJson(formData) {
    var obj = {};

    formData.forEach(function (v, i) {
        obj[i] = v;
    });

    return JSON.stringify(obj);
}