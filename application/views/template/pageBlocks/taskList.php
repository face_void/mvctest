<?if (empty($data['ITEMS'])):?>
    <div class="row justify-content-center">Список задач пуст</div>
<?else:?>
    <div class="row sort">
        <form name="taskSort" action="/main/taskSort">
            <?
            $sortField = $_SESSION['taskSort'] ?: false;
            $sortDirection = $_SESSION['taskSortDirection'] ?: false;

            $arSortFields = [
                'null' => [
                    'NAME' => 'Выберите сортировку'
                ],
                'userName|asc' => [
                    'NAME' => 'Имя пользователя по возрастанию'
                ],
                'userName|desc' => [
                    'NAME' => 'Имя пользователя по убыванию'
                ],
                'userEmail|asc' => [
                    'NAME' => 'Email по возрастанию'
                ],
                'userEmail|desc' => [
                    'NAME' => 'Email по убыванию'
                ],
                'completed|desc' => [
                    'NAME' => 'Статус задачи по возрастанию'
                ],
                'completed|asc' => [
                    'NAME' => 'Статус задачи по убыванию'
                ],
            ];

            if ($sortField && $sortDirection) {
                unset($arSortFields['null']);
                $arSortFields["$sortField|$sortDirection"]['SELECTED'] = 'Y';
            }
            ?>
            <label>Сортировка:
                <select class="js-sort" name="sortField">
                    <?foreach ($arSortFields as $k => $arField):?>
                        <option value="<?=$k?>"<?if ($arField['SELECTED'] == 'Y'):?> selected<?endif?>><?=$arField['NAME']?></option>
                    <?endforeach?>
                </select>
            </label>
        </form>
    </div>
    <div class="row taskList__head">
        <div class="col-sm-2 align-items-center justify-content-center">Имя пользователя</div>
        <div class="col-sm-2 align-items-center justify-content-center">Email</div>
        <div class="col-sm-4 align-items-center justify-content-center">Текст задачи</div>
        <div class="col-sm-2 align-items-center justify-content-center">Выполнено</div>
        <div class="col-sm-2 align-items-center justify-content-center">Отредактировано администратором</div>
    </div>
    <?foreach ($data['ITEMS'] as $arTask):?>
        <div class="row task">
            <div class="col-sm-2"><?=$arTask['USER_NAME']?></div>
            <div class="col-sm-2"><?=$arTask['USER_EMAIL']?></div>
            <div class="col-sm-4"><?=$arTask['TEXT']?></div>
            <div class="col-sm-2">
                <?if ($arTask['COMPLETED'] == 'Y'):?>
                    <div class="row h-100 align-items-center justify-content-center">
                        <svg class="bi bi-check" width="1em" height="1em" viewBox="0 0 16 16" fill="green" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M13.854 3.646a.5.5 0 010 .708l-7 7a.5.5 0 01-.708 0l-3.5-3.5a.5.5 0 11.708-.708L6.5 10.293l6.646-6.647a.5.5 0 01.708 0z" clip-rule="evenodd"/>
                        </svg>
                    </div>
                <?endif?>
            </div>
            <div class="col-sm-2">
                <?if ($arTask['EDITED'] == 'Y'):?>
                    <div class="row h-100 align-items-center justify-content-center">
                        <svg class="bi bi-check" width="1em" height="1em" viewBox="0 0 16 16" fill="green" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M13.854 3.646a.5.5 0 010 .708l-7 7a.5.5 0 01-.708 0l-3.5-3.5a.5.5 0 11.708-.708L6.5 10.293l6.646-6.647a.5.5 0 01.708 0z" clip-rule="evenodd"/>
                        </svg>
                    </div>
                <?endif?>
            </div>
        </div>
    <?endforeach?>
    <?if ($data['PAGINATION']['MAX_PAGE'] > 1):?>
        <div class="row pagination justify-content-center">
            <a href="/main/prevPage" class="pagination__item<?if ($data['PAGINATION']['CUR_PAGE'] != $data['PAGINATION']['MAX_PAGE']):?> active<?endif?>">
                <svg class="bi bi-arrow-left-short" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M7.854 4.646a.5.5 0 010 .708L5.207 8l2.647 2.646a.5.5 0 01-.708.708l-3-3a.5.5 0 010-.708l3-3a.5.5 0 01.708 0z" clip-rule="evenodd"/>
                    <path fill-rule="evenodd" d="M4.5 8a.5.5 0 01.5-.5h6.5a.5.5 0 010 1H5a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
                </svg>
            </a>
            <?
            $page = 1;

            while ($page <= $data['PAGINATION']['MAX_PAGE']):?>
                <a href="/main/setPage" class="pagination__item<?if ($page == $data['PAGINATION']['CUR_PAGE']):?> active<?endif?>"><?=$page?></a>
            <?
            $page++;
            endwhile;?>
            <a href="/main/nextPage" class="pagination__item<?if ($data['PAGINATION']['CUR_PAGE'] == $data['PAGINATION']['MAX_PAGE']):?> active<?endif?>">
                <svg class="bi bi-arrow-right-short" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8.146 4.646a.5.5 0 01.708 0l3 3a.5.5 0 010 .708l-3 3a.5.5 0 01-.708-.708L10.793 8 8.146 5.354a.5.5 0 010-.708z" clip-rule="evenodd"/>
                    <path fill-rule="evenodd" d="M4 8a.5.5 0 01.5-.5H11a.5.5 0 010 1H4.5A.5.5 0 014 8z" clip-rule="evenodd"/>
                </svg>
            </a>
        </div>
    <?endif?>
<?endif?>
