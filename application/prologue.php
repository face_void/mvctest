<?
session_start();
// подключаем классы ядра
require_once 'core/Model.php';
require_once 'core/View.php';
require_once 'core/Controller.php';
require_once 'core/Route.php';
require_once 'core/Connector.php';

// подключаем классы дополнительных библиотек
require_once 'core/lib/Validator.php';

// constants
define('TEMPLATE_PATH', '/application/views/template');

// database
$GLOBALS['data']['db'] = Connector::connect();