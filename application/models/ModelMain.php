<?
class ModelMain extends Model
{
    /**
     * @var mysqli
     */
    private $db;

    const TASKS_COUNT = 3;

    public function __construct()
    {
        $this->db = $GLOBALS['data']['db'];
    }

    /**
     * Возвращает список задач
     *
     * @return array
     * @throws Exception
     */
    public function getTasks()
    {
        // сортировка по уполчанию - айди по убыванию
        $sort = (!empty($_SESSION['taskSort'])) ? $_SESSION['taskSort'] : 'id';
        $direction = (!empty($_SESSION['taskSortDirection'])) ? $_SESSION['taskSortDirection'] : 'desc';
        $direction = strtoupper($direction);

        // настройки пагинации
        if (empty($_SESSION['taskCurPage'])) {
            $curPage = $_SESSION['taskCurPage'] = 1;
        } else {
            $curPage = $_SESSION['taskCurPage'];
        }
        $tasksCount = self::TASKS_COUNT;
        $skipRows = ($curPage - 1) * $tasksCount;

        $sql = "SELECT * FROM tasks ORDER BY $sort $direction LIMIT $skipRows, $tasksCount";

        if (!$result = $this->db->query($sql)) {
            throw new \Exception('Возникла проблема при получении списка задач');
        }

        $arTasks = [];
        while ($arTask = $result->fetch_assoc()) {
            $arTasks['ITEMS'][] = [
                'ID' => $arTask['id'],
                'USER_NAME' => $arTask['userName'],
                'USER_EMAIL' => $arTask['userEmail'],
                'TEXT' => $arTask['taskText'],
                'COMPLETED' => $arTask['completed'],
                'EDITED' => $arTask['edited']
            ];
        }

        $sqlPagination = "SELECT COUNT(*) FROM tasks";

        if (!$resultPagination = $this->db->query($sqlPagination)) {
            throw new \Exception('Возникла проблема во время настройки пагинации');
        }

        $paginationData = $resultPagination->fetch_row();
        $maxPage = ceil($paginationData[0] / $tasksCount);

        $arTasks['PAGINATION'] = [
            'CUR_PAGE' => $curPage,
            'MAX_PAGE' => $maxPage,
            'ITEMS_COUNT' => $tasksCount
        ];

        return $arTasks;
    }

    /**
     * Создаёт новую задачу
     *
     * @return mixed
     * @throws Exception
     */
    public function createTask()
    {
        // @todo нужно добавить проверку по какому-то session id, для защиты от автогенерации левых задач
        $request = $this->prepareRequest($_REQUEST['data']);

        $return = ['error' => false];

        // проверка заполненности полей
        $arRequireFields = ['userName', 'userEmail', 'taskText'];
        $arMissedFields = [];
        foreach ($arRequireFields as $v) {
            if (empty($request[ $v ])) {
                $arMissedFields[] = $v;
                continue;
            }
        }

        if (!empty($arMissedFields)) {
            $return['error'] = true;
            $return['missedFields'] = $arMissedFields;
        }

        // валидация полей (только email)
        if (!Validator::validateEmail($request['userEmail'])) {
            $return['error'] = true;
            $return['notValidFields'] = 'userEmail';
        }

        // если нет ошибок - создаём заявку
        if (!$return['error']) {
            $taskData = [
                'userName' => $request['userName'],
                'userEmail' => $request['userEmail'],
                'taskText' => $request['taskText']
            ];

            $sql = "INSERT INTO tasks (userName, userEmail, taskText) VALUES ('{$taskData['userName']}', '{$taskData['userEmail']}', '{$taskData['taskText']}')";

            if (!$result = $this->db->query($sql)) {
                throw new \Exception('Возникла проблема при создании задачи');
            }

            $return['taskId'] = $this->db->insert_id;
            $this->db->close();
        }

        return $return;
    }

    /**
     * Редактирование заявки
     *
     * @return array
     * @throws Exception
     */
    public function editTask()
    {
        $return = ['error' => false];

        // проверяем, залогинен ли пользователь
        if ($_SESSION['user']['admin'] != 'Y') {
            $return['error'] = true;
            $return['message'][] = 'Необходима авторизация';
        } else {
            $request = $this->prepareRequest($_REQUEST['data']);

            // Проверяем, был ли изменён текст задачи
            $isEditSql = "SELECT id FROM tasks WHERE id = '{$request['taskId']}' AND taskText='{$request['taskText']}'";

            if (!$isEditResult = $this->db->query($isEditSql)) {
                throw new \Exception('Возникла проблема при проверке задачи');
            }

            $isEdit = ($isEditResult->num_rows > 0) ? false : true;

            $sql = "UPDATE tasks 
                SET taskText = '{$request['taskText']}'";

            if (!empty($request['completed']))
                $sql .= ", completed = 'Y'";
            else
                $sql .= ", completed = 'N'";

            if ($isEdit)
                $sql .= ", edited = 'Y'";

            $sql .= " WHERE id = '{$request['taskId']}'";

            if (!$result = $this->db->query($sql)) {
                throw new \Exception('Возникла проблема при редактировании задачи');
            }
        }

        return $return;
    }

    /**
     * Авторизация
     */
    public function authorize()
    {
        $return = ['error' => false, 'missedFields' => [], 'message' => []];
        $request = $this->prepareRequest($_REQUEST['data']);

        // проверка заполненности полей
        $arRequireFields = ['userName', 'userPassword'];
        $arMissedFields = [];
        foreach ($arRequireFields as $v) {
            if (empty($request[ $v ])) {
                $arMissedFields[] = $v;
                continue;
            }
        }

        if (!empty($arMissedFields)) {
            $return['error'] = true;
            $return['missedFields'] = $arMissedFields;
        }

        if (!$return['error']) {
            $sql = "SELECT * FROM users WHERE name = '{$request['userName']}' AND password = '{$request['userPassword']}'";

            if (!$result = $this->db->query($sql)) {
                throw new \Exception('Возникла проблема при авторизации');
            }

            // пользователь не найден
            if ($result->num_rows == 0) {
                $return['error'] = true;
                $return['message'][] = 'Пользователь не найден';
            }

            // пользователь найден и только один
            if ($result->num_rows == 1) {
                $arUser = $result->fetch_assoc();

                $_SESSION['user'] = [
                    'id' => $arUser['id'],
                    'admin' => 'Y'
                ];
            } elseif ($result->num_rows > 1) {
                // почему-то больше одного пользователя с заданными параметрами

                $return['error'] = true;
                $return['message'][] = 'Ошибка в базе данных. Обратитесь к администратору';
            }
        }

        return $return;
    }

    /**
     * Отмена авторизации
     */
    public function logout()
    {
        unset($_SESSION['user']);

        return ['error' => false];
    }

    /**
     * Подготавливает параметры запроса
     *
     * @param $data
     * @return array
     */
    public function prepareRequest($data)
    {
        $requestData = json_decode($data, 1);
        $request = [];
        foreach ($requestData as $k => $v) {
            $request[ $k ] = htmlspecialchars(strip_tags($v), ENT_QUOTES);
        }

        return $request;
    }
}